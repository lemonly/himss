$(document).ready(function() {
    $('.overview__bar').click(overviewOpen);
    $('.buttons__button').click(buttonShow);

    $('.frame__corner').click(function() {
        if (!$('#lemonMade').hasClass('dashboard')) {
            $('#lemonMade').addClass('dashboard');
            $('.body__back').hide();
            $('.body__blueprint').hide();
            $('.body__gradient').hide();
            $('.buttons__button').removeClass('show');
            $('.body__net').attr('style', 'transform: translate(0, 0)');

            $('.frame__arrow').removeClass('show');
            $('.frame__arrow p span').html('');

            $('.body__frame').removeClass('text-shown');
            $('.body__net').removeClass('text-shown');
        }
    });

    $('.body__back').click(function() {
        if (!$('#lemonMade').hasClass('dashboard')) {
            $('#lemonMade').addClass('dashboard');
            $('.body__back').hide();
            $('.body__blueprint').hide();
            $('.body__gradient').hide();
            $('.buttons__button').removeClass('show');
            $('.body__net').attr('style', 'transform: translate(0, 0)');

            $('.frame__arrow').removeClass('show');
            $('.frame__arrow p span').html('');

            $('.body__frame').removeClass('text-shown');
            $('.body__net').removeClass('text-shown');
        }
    });

    $('body').on('mouseenter', '.body__dashboard #launchPrivia', function() {
        $('#priviaText').css('fill', '#B6BF34');
    });

    $('body').on('mouseout', '.body__dashboard #launchPrivia', function() {
        $('#priviaText').css('fill', '#ffffff');
    });

    $('body').on('mouseenter', '.body__dashboard #launchEasterseals', function() {
        $('#eastersealsText').css('fill', '#B6BF34');
    });

    $('body').on('mouseout', '.body__dashboard #launchEasterseals', function() {
        $('#eastersealsText').css('fill', '#ffffff');
    });

    $('body').on('mouseenter', '.body__dashboard #launchAdventhealth', function() {
        $('#adventHealthText').css('fill', '#B6BF34');
    });

    $('body').on('mouseout', '.body__dashboard #launchAdventhealth', function() {
        $('#adventHealthText').css('fill', '#ffffff');
    });

    $('body').on('click', '.body__dashboard #launchPrivia', function() {
        $('#lemonMade').removeClass('dashboard');

        $('.body__back').show();
        $('.body__blueprint').show();
        $('.body__gradient').show();

        $('.buttons--container').hide();
        $('.buttons--container.privia').show();

        $('.body__net object').hide();
        $('.body__net object.privia').show();

        $('.bar__client').hide();
        $('.bar__client.privia').show();

        $('.content__main').hide();
        $('.content__main.privia').show();

        $('.content__sub').hide();
        $('.content__sub.privia').show();

        var rightClientArrowWidth = ($('.overview__bar').width() / 2) - (($('.bar__client').width() / 2) + 20) - 80;
        $('.client__arrow-right').width(rightClientArrowWidth);
    });

    $('body').on('click', '.body__dashboard #launchAdventhealth', function() {
        $('#lemonMade').removeClass('dashboard');

        $('.body__back').show();
        $('.body__blueprint').show();
        $('.body__gradient').show();

        $('.buttons--container').hide();
        $('.buttons--container.adventhealth').show();

        $('.body__net object').hide();
        $('.body__net object.adventhealth').show();

        $('.bar__client').hide();
        $('.bar__client.adventhealth').show();

        $('.content__main').hide();
        $('.content__main.adventhealth').show();

        $('.content__sub').hide();
        $('.content__sub.adventhealth').show();

        var rightClientArrowWidth = ($('.overview__bar').width() / 2) - (($('.bar__client').width() / 2) + 20) - 116;
        $('.client__arrow-right').width(rightClientArrowWidth);
    });

    $('body').on('click', '.body__dashboard #launchEasterseals', function() {
        $('#lemonMade').removeClass('dashboard');

        $('.body__back').show();
        $('.body__blueprint').show();
        $('.body__gradient').show();

        $('.buttons--container').hide();
        $('.buttons--container.easterseals').show();

        $('.body__net object').hide();
        $('.body__net object.easterseals').show();

        $('.bar__client').hide();
        $('.bar__client.easterseals').show();

        $('.content__main').hide();
        $('.content__main.easterseals').show();

        $('.content__sub').hide();
        $('.content__sub.easterseals').show();

        var rightClientArrowWidth = ($('.overview__bar').width() / 2) - (($('.bar__client').width() / 2) + 20) - 140;
        $('.client__arrow-right').width(rightClientArrowWidth);
    });

    $('body').on('click', '.frame__arrow', function() {
        $('.frame__arrow').removeClass('show');
        $('.frame__arrow p span').html('');

        $('.body__frame').removeClass('text-shown');
        $('.body__net').removeClass('text-shown');

        $('.body__net').attr('style', 'transform: translate(0, 0)');
    });
});

$(window).on('load', function() {
    $($('#toggleOverview')[0].contentDocument).find('.toggle').click(overviewOpen);
    $('.buttons__button object').each(function() {
        $($(this)[0].contentDocument).find('.toggle-plus').click(buttonShow);
    });
    $('.overview__bar').addClass('show');

    var rightClientArrowWidth = ($('.overview__bar').width() / 2) - (($('.bar__client').width() / 2) + 20) - 80;
    $('.client__arrow-right').width(rightClientArrowWidth);

    $($('#dashboardGraphic')[0].contentDocument).find('#launchPrivia')
});

$(window).on('resize', function() {
    if ($('.buttons--container.privia').is(':visible')) {
        var rightClientArrowWidth = ($('.overview__bar').width() / 2) - (($('.bar__client').width() / 2) + 20) - 80;
    } else if ($('.buttons--container.easterseals').is(':visible')) {
        var rightClientArrowWidth = ($('.overview__bar').width() / 2) - (($('.bar__client').width() / 2) + 20) - 140;
    } else {
        var rightClientArrowWidth = ($('.overview__bar').width() / 2) - (($('.bar__client').width() / 2) + 20) - 116;
    }

    $('.client__arrow-right').width(rightClientArrowWidth);
});

function overviewOpen() {
    $($('#toggleOverview')[0].contentDocument).find('.arrow').toggleClass('show');
    $($('#toggleOverview')[0].contentDocument).find('.vertical-line').toggleClass('show');
    $('.body__overview').toggleClass('show');
}

var popupText = {
    growthPrivia: "Privia went live with our solutions in early 2014 with 40 providers across several locations in 1 state. Now, they have 1,600 providers in 5 states with 400+ locations.",
    interoperability: "150+ out-of-the box global interfaces allows Privia to get connected quickly, giving them unprecedented integration with robust, innovative applications. Standard and free of charge when any practice goes live with athenaNet, these global interfaces allow the exchange of clinical and financial data within and outside of Privia’s network across most of the U.S.",
    qualityData: "With custom portal messaging on flu vaccine, pneumonia vaccine, colon cancer screening, and breast cancer screening, Privia closed 12,000 care gaps, saved 5 minutes of time per gap closed, and contacted patients in need of care; 89% email open rate.",
    patientCommunication: "Privia used athenahealth’s Platform Services to seamlessly connect their own online scheduling app to athenaNet; 1,200 out of 1,600 (75%) of providers are live on Find A Doctor; 15 percent of all appointments scheduled through Find A Doctor.",
    providers: "",
    marketplace: "Privia leverages proprietary apps they’ve built on our Platform Services to create a unified health IT experience for patients and providers. Ultimately, they may sell their proprietary in-house developed apps, and distribute those on our Marketplace, for new revenue streams in the future.",
    growthEasterseals: "40x growth in 6 years: $6 million, 200 hundred practitioner to almost $300 million, 8,000+ practitioner organization",
    packagedSolutionSets: "ESBA is creating revenue streams using our Platform Services to build and package proprietary applications and services for other organizations with similar workflow challenges.",
    patients: "ESBA is building a mobile app on top of our platform that allows families and caretakers to:<br><ul><li>Schedule appointments</li><li>Make payments</li><li>Enter and track real-time health data through wearable devices</li></ul>",
    esbaPractitioners: "First in the industry: New applications standardize and capture financial, clinical, compliance details from each behavioral health home visit",
    growthAdventHealth: "",
    clinicalDecisionSupport: "We integrate clinical decision support right into the workflow to make it easy for providers to give safe, effective care.",
    referralPlatform: "We built an integrated referral system that gives providers an easy way to create and track referrals, while encouraging the use of low cost, high quality providers.",
    emrIntegration: "With the help of our Platform Services, AdventHealth clinicians have on-demand access to the full patient chart across ambulatory and inpatient settings. Smart data replication ensures the information surfaced in each care setting is meaningful to the providers using it, regardless of the EHR in which it originated. That means alerting physicians when their patients are seen in an inpatient setting and making data like patient history and physical information available to clinicians in the hospital.",
    patientsAdventHealth: "AdventHealth connects its own patient portal to our platform so that patients have a convenient, self-serve application to use for appointments, payments, and provider communications.",
    financial: "AdventHealth has a unified financial management system that integrates coding, billing and revenue cycle management across inpatient and ambulatory settings, giving leadership visibility into and control over the financial health of the enterprise."
};

function buttonShow(e) {
    $('.body__net').attr('style', 'transform: translate(0, 0)');

    var currentTarget = $(e.currentTarget);
    var currentTargetShown = currentTarget.hasClass('show');

    $('.buttons__button').removeClass('show');

    var graphic = $($('.body__net object:visible')[0].contentDocument);
    var showElements = currentTarget.attr('id');
    var graphicElements = graphic.find('.' + showElements);

    var newText = popupText[showElements];

    $('.frame__arrow').removeClass('show');
    $('.frame__arrow p span').html('');

    $('.body__frame').removeClass('text-shown');
    $('.body__net').removeClass('text-shown');

    graphic.find('#graphics > g').each(function() {
        $(this).attr('style', 'opacity: 1');
        if ($(this).attr('class')) {
            if ($(this).attr('class').search('show') != -1) {
                $(this).attr('class', $(this).attr('class').slice(0, -5));
            }
        }
        if ($(this).hasClass('tile')) {
            var linearGradients = $(this).find('linearGradient');
            linearGradients.each(function() {
                $(this).find('stop').each(function() {
                    if ($(this).attr('style') == 'stop-color:#ce5995') {
                        $(this).attr('style', 'stop-color:#B6BF34');
                    }
                    if ($(this).attr('style') == 'stop-color:#a51d53') {
                        $(this).attr('style', 'stop-color:#7A9B3D');
                    }
                    if ($(this).attr('style') == 'stop-color:#ce5996') {
                        $(this).attr('style', 'stop-color:#8C6DA9');
                    }
                    if ($(this).attr('style') == 'stop-color:#a51d54') {
                        $(this).attr('style', 'stop-color:#582D82');
                    }
                    if ($(this).attr('style') == 'stop-color:#A51D52') {
                        $(this).attr('style', 'stop-color:#582D83');
                    }
                    if ($(this).attr('style') == 'stop-color:#900A53') {
                        $(this).attr('style', 'stop-color:#582C83');
                    }
                    if ($(this).attr('style') == 'stop-color:#C34D86') {
                        $(this).attr('style', 'stop-color:#371358');
                    }
                });
            });
            var paths = $(this).find('path');
            var thisElement = $(this);
            paths.each(function() {
                if (thisElement.hasClass('white')) {
                    $(this).css('fill', '#694290');
                } else {
                    if ($(this).css('fill') == 'rgb(206, 89, 149)') {
                        $(this).css('fill', 'rgb(219, 223, 135)');
                    }
                    if ($(this).css('fill') == 'rgb(165, 29, 83)') {
                        $(this).css('fill', 'rgb(182, 191, 52)');
                    }
                    if ($(this).css('fill') == 'rgb(206, 89, 150)') {
                        $(this).css('fill', 'rgb(149, 174, 52)');
                    }
                    if ($(this).css('fill') == 'rgb(165, 29, 84)') {
                        $(this).css('fill', 'rgb(88, 45, 131)');
                    }
                    if ($(this).css('fill') == 'rgb(206, 89, 148)') {
                        $(this).css('fill', 'rgb(138, 107, 168)');
                    }
                    if ($(this).css('fill') == 'rgb(165, 29, 82)') {
                        $(this).css('fill', 'rgb(192, 199, 78)');
                    }
                    if ($(this).css('fill') == 'rgb(236, 135, 176)') {
                        $(this).css('fill', 'rgb(122, 155, 61)');
                    }
                }
            });
            var rects = $(this).find('rect');
            rects.each(function() {
                if (thisElement.hasClass('white')) {
                    $(this).css('fill', '#694290');
                } else {
                    if ($(this).css('fill') == 'rgb(206, 89, 149)') {
                        $(this).css('fill', 'rgb(219, 223, 135)');
                    }
                    if ($(this).css('fill') == 'rgb(165, 29, 83)') {
                        $(this).css('fill', 'rgb(182, 191, 52)');
                    }
                    if ($(this).css('fill') == 'rgb(206, 89, 150)') {
                        $(this).css('fill', 'rgb(149, 174, 52)');
                    }
                    if ($(this).css('fill') == 'rgb(165, 29, 84)') {
                        $(this).css('fill', 'rgb(88, 45, 131)');
                    }
                    if ($(this).css('fill') == 'rgb(206, 89, 148)') {
                        $(this).css('fill', 'rgb(138, 107, 168)');
                    }
                    if ($(this).css('fill') == 'rgb(165, 29, 82)') {
                        $(this).css('fill', 'rgb(192, 199, 78)');
                    }
                }
            });
            var polygons = $(this).find('polygon');
            polygons.each(function() {
                if (thisElement.hasClass('white')) {
                    $(this).css('fill', '#694290');
                } else {
                    if ($(this).css('fill') == 'rgb(206, 89, 149)') {
                        $(this).css('fill', 'rgb(219, 223, 135)');
                    }
                    if ($(this).css('fill') == 'rgb(165, 29, 83)') {
                        $(this).css('fill', 'rgb(182, 191, 52)');
                    }
                    if ($(this).css('fill') == 'rgb(206, 89, 150)') {
                        $(this).css('fill', 'rgb(149, 174, 52)');
                    }
                    if ($(this).css('fill') == 'rgb(165, 29, 84)') {
                        $(this).css('fill', 'rgb(88, 45, 131)');
                    }
                    if ($(this).css('fill') == 'rgb(206, 89, 148)') {
                        $(this).css('fill', 'rgb(138, 107, 168)');
                    }
                    if ($(this).css('fill') == 'rgb(165, 29, 82)') {
                        $(this).css('fill', 'rgb(192, 199, 78)');
                    }
                }
            });
            var polylines = $(this).find('polyline');
            polylines.each(function() {
                if (thisElement.hasClass('white')) {
                    $(this).css('fill', '#694290');
                } else {
                    if ($(this).css('fill') == 'rgb(206, 89, 149)') {
                        $(this).css('fill', 'rgb(219, 223, 135)');
                    }
                    if ($(this).css('fill') == 'rgb(165, 29, 83)') {
                        $(this).css('fill', 'rgb(182, 191, 52)');
                    }
                    if ($(this).css('fill') == 'rgb(206, 89, 150)') {
                        $(this).css('fill', 'rgb(149, 174, 52)');
                    }
                    if ($(this).css('fill') == 'rgb(165, 29, 84)') {
                        $(this).css('fill', 'rgb(88, 45, 131)');
                    }
                    if ($(this).css('fill') == 'rgb(206, 89, 148)') {
                        $(this).css('fill', 'rgb(138, 107, 168)');
                    }
                    if ($(this).css('fill') == 'rgb(165, 29, 82)') {
                        $(this).css('fill', 'rgb(192, 199, 78)');
                    }
                }
            });
        }
    });

    graphic.find('#text > g').each(function() {
        $(this).attr('style', 'opacity: 1');
        if ($(this).attr('class')) {
            if ($(this).attr('class').search('show') != -1) {
                $(this).attr('class', $(this).attr('class').slice(0, -5));
            }
        }
        if ($(this).hasClass('magenta')) {
            $(this).find('text').first().attr('style', 'fill: #B6BF34 !important;');
        }
    });

    if (currentTargetShown) {
        currentTarget.removeClass('show');

        graphic.find('#graphics > g').each(function() {
            if (!$(this).hasClass(showElements)) {
                $(this).attr('style', 'opacity: 1');
            } else {
                var thisElement = $(this);
                if (thisElement.hasClass('tile')) {
                    var linearGradients = $(this).find('linearGradient');
                    linearGradients.each(function() {
                        $(this).find('stop').each(function() {
                            if ($(this).attr('style') == 'stop-color:#ce5995') {
                                $(this).attr('style', 'stop-color:#B6BF34');
                            }
                            if ($(this).attr('style') == 'stop-color:#a51d53') {
                                $(this).attr('style', 'stop-color:#7A9B3D');
                            }
                            if ($(this).attr('style') == 'stop-color:#ce5996') {
                                $(this).attr('style', 'stop-color:#8C6DA9');
                            }
                            if ($(this).attr('style') == 'stop-color:#a51d54') {
                                $(this).attr('style', 'stop-color:#582D82');
                            }
                            if ($(this).attr('style') == 'stop-color:#A51D52') {
                                $(this).attr('style', 'stop-color:#582D83');
                            }
                            if ($(this).attr('style') == 'stop-color:#900A53') {
                                $(this).attr('style', 'stop-color:#582C83');
                            }
                            if ($(this).attr('style') == 'stop-color:#C34D86') {
                                $(this).attr('style', 'stop-color:#371358');
                            }
                        });
                    });
                    var paths = $(this).find('path');
                    paths.each(function() {
                        if ($(this).css('fill') == 'rgb(206, 89, 149)') {
                            $(this).css('fill', 'rgb(219, 223, 135)');
                        }
                        if ($(this).css('fill') == 'rgb(165, 29, 83)') {
                            $(this).css('fill', 'rgb(182, 191, 52)');
                        }
                        if ($(this).css('fill') == 'rgb(206, 89, 150)') {
                            $(this).css('fill', 'rgb(149, 174, 52)');
                        }
                        if ($(this).css('fill') == 'rgb(165, 29, 84)') {
                            $(this).css('fill', 'rgb(88, 45, 131)');
                        }
                        if ($(this).css('fill') == 'rgb(206, 89, 148)') {
                            $(this).css('fill', 'rgb(138, 107, 168)');
                        }
                        if ($(this).css('fill') == 'rgb(165, 29, 82)') {
                            $(this).css('fill', 'rgb(192, 199, 78)');
                        }
                        if ($(this).css('fill') == 'rgb(236, 135, 176)') {
                            $(this).css('fill', 'rgb(122, 155, 61)');
                        }
                    });
                    var polylines = $(this).find('polyline');
                    polylines.each(function() {
                        if ($(this).css('fill') == 'rgb(206, 89, 149)') {
                            $(this).css('fill', 'rgb(219, 223, 135)');
                        }
                        if ($(this).css('fill') == 'rgb(165, 29, 83)') {
                            $(this).css('fill', 'rgb(182, 191, 52)');
                        }
                        if ($(this).css('fill') == 'rgb(206, 89, 150)') {
                            $(this).css('fill', 'rgb(149, 174, 52)');
                        }
                        if ($(this).css('fill') == 'rgb(165, 29, 84)') {
                            $(this).css('fill', 'rgb(88, 45, 131)');
                        }
                        if ($(this).css('fill') == 'rgb(206, 89, 148)') {
                            $(this).css('fill', 'rgb(138, 107, 168)');
                        }
                        if ($(this).css('fill') == 'rgb(165, 29, 82)') {
                            $(this).css('fill', 'rgb(192, 199, 78)');
                        }
                        if ($(this).css('fill') == 'rgb(236, 135, 176)') {
                            $(this).css('fill', 'rgb(122, 155, 61)');
                        }
                    });
                    var rects = $(this).find('rect');
                    rects.each(function() {
                        if ($(this).css('fill') == 'rgb(206, 89, 149)') {
                            $(this).css('fill', 'rgb(219, 223, 135)');
                        }
                        if ($(this).css('fill') == 'rgb(165, 29, 83)') {
                            $(this).css('fill', 'rgb(182, 191, 52)');
                        }
                        if ($(this).css('fill') == 'rgb(206, 89, 150)') {
                            $(this).css('fill', 'rgb(149, 174, 52)');
                        }
                        if ($(this).css('fill') == 'rgb(165, 29, 84)') {
                            $(this).css('fill', 'rgb(88, 45, 131)');
                        }
                        if ($(this).css('fill') == 'rgb(206, 89, 148)') {
                            $(this).css('fill', 'rgb(138, 107, 168)');
                        }
                        if ($(this).css('fill') == 'rgb(165, 29, 82)') {
                            $(this).css('fill', 'rgb(192, 199, 78)');
                        }
                    });
                    var polygons = $(this).find('polygon');
                    polygons.each(function() {
                        if ($(this).css('fill') == 'rgb(206, 89, 149)') {
                            $(this).css('fill', 'rgb(219, 223, 135)');
                        }
                        if ($(this).css('fill') == 'rgb(165, 29, 83)') {
                            $(this).css('fill', 'rgb(182, 191, 52)');
                        }
                        if ($(this).css('fill') == 'rgb(206, 89, 150)') {
                            $(this).css('fill', 'rgb(149, 174, 52)');
                        }
                        if ($(this).css('fill') == 'rgb(165, 29, 84)') {
                            $(this).css('fill', 'rgb(88, 45, 131)');
                        }
                        if ($(this).css('fill') == 'rgb(206, 89, 148)') {
                            $(this).css('fill', 'rgb(138, 107, 168)');
                        }
                        if ($(this).css('fill') == 'rgb(165, 29, 82)') {
                            $(this).css('fill', 'rgb(192, 199, 78)');
                        }
                    });
                } else if (thisElement.hasClass('white')) {
                   var paths = $(this).find('path');
                   paths.each(function() {
                       $(this).attr('style', 'fill: #6D4793 !important');
                   });
                   var rects = $(this).find('rect');
                   rects.each(function() {
                       $(this).attr('style', 'fill: #6D4793 !important');
                   });
                   var polygons = $(this).find('polygon');
                   polygons.each(function() {
                       $(this).attr('style', 'fill: #6D4793 !important');
                   });
               }
            }
        });

        graphic.find('#text > g').each(function() {
            if (!$(this).hasClass(showElements)) {
                $(this).attr('style', 'opacity: 1');
            }
            if ($(this).hasClass('tile')) {
                var linearGradients = $(this).find('linearGradient');
                linearGradients.each(function() {
                    $(this).find('stop').each(function() {
                        if ($(this).attr('style') == 'stop-color:#ce5995') {
                            $(this).attr('style', 'stop-color:#B6BF34');
                        }
                        if ($(this).attr('style') == 'stop-color:#a51d53') {
                            $(this).attr('style', 'stop-color:#7A9B3D');
                        }
                    });
                });
            }
            if (showElements == 'growthEasterseals' && $(this).hasClass('magenta')) {
                $(this).find('text').first().attr('style', 'fill: #B6BF34 !important;');
            }
        });
    } else {
        if (newText.length > 0) {
            $('.frame__arrow').addClass('show');
            $('.frame__arrow p span').html(newText);

            if (!currentTarget.hasClass('no-move')) {
                $('.body__net').attr('style', 'transform: translate(0, ' + ($('.frame__arrow').height() + 20) + 'px)');
            }
        }

        $('.body__frame').addClass('text-shown');
        $('.body__net').addClass('text-shown');

        currentTarget.addClass('show');
        graphicElements.each(function() {
            $(this).attr('class', $(this).attr('class') + ' show');
        });

        graphic.find('#graphics > g').each(function() {
            if (!$(this).hasClass(showElements)) {
                $(this).attr('style', 'opacity: 0.3');
            } else {
                var thisElement = $(this);
                if ($(this).hasClass('tile')) {
                    var linearGradients = $(this).find('linearGradient');
                    linearGradients.each(function() {
                        $(this).find('stop').each(function() {
                            if (!thisElement.hasClass('middle')) {
                                if ($(this).attr('style') == 'stop-color:#8C6DA9') {
                                    $(this).attr('style', 'stop-color:#ce5996');
                                }
                                if ($(this).attr('style') == 'stop-color:#582D82') {
                                    $(this).attr('style', 'stop-color:#a51d54');
                                }
                                if ($(this).attr('style') == 'stop-color:#582D83') {
                                    $(this).attr('style', 'stop-color:#A51D52');
                                }
                                if ($(this).attr('style') == 'stop-color:#582C83') {
                                    $(this).attr('style', 'stop-color:#900A53');
                                }
                                if ($(this).attr('style') == 'stop-color:#371358') {
                                    $(this).attr('style', 'stop-color:#C34D86');
                                }
                            }
                            if ($(this).attr('style') == 'stop-color:#B6BF34') {
                                $(this).attr('style', 'stop-color:#ce5995');
                            }
                            if ($(this).attr('style') == 'stop-color:#7A9B3D') {
                                $(this).attr('style', 'stop-color:#a51d53');
                            }
                        });
                    });
                    var paths = $(this).find('path');
                    paths.each(function() {
                        if ($(this).css('fill') == 'rgb(182, 191, 52)') {
                            $(this).css('fill', 'rgb(165, 29, 83)');
                        }
                        if ($(this).css('fill') == 'rgb(219, 223, 135)') {
                            $(this).css('fill', 'rgb(206, 89, 149)');
                        }
                        if ($(this).css('fill') == 'rgb(192, 199, 78)') {
                            $(this).css('fill', 'rgb(165, 29, 82)');
                        }
                        if ($(this).css('fill') == 'rgb(122, 155, 61)') {
                            $(this).css('fill', 'rgb(236, 135, 176)');
                        }
                        if ($(this).css('fill') == 'rgb(149, 174, 52)') {
                            $(this).css('fill', 'rgb(206, 89, 150)');
                        }
                        if ($(this).css('fill') == 'rgb(88, 45, 131)') {
                            $(this).css('fill', 'rgb(165, 29, 84)');
                        }
                        if ($(this).css('fill') == 'rgb(138, 107, 168)') {
                            $(this).css('fill', 'rgb(206, 89, 148)');
                        }
                    });
                    var polylines = $(this).find('polyline');
                    polylines.each(function() {
                        if ($(this).css('fill') == 'rgb(182, 191, 52)') {
                            $(this).css('fill', 'rgb(165, 29, 83)');
                        }
                        if ($(this).css('fill') == 'rgb(219, 223, 135)') {
                            $(this).css('fill', 'rgb(206, 89, 149)');
                        }
                        if ($(this).css('fill') == 'rgb(192, 199, 78)') {
                            $(this).css('fill', 'rgb(165, 29, 82)');
                        }
                        if ($(this).css('fill') == 'rgb(122, 155, 61)') {
                            $(this).css('fill', 'rgb(236, 135, 176)');
                        }
                        if ($(this).css('fill') == 'rgb(149, 174, 52)') {
                            $(this).css('fill', 'rgb(206, 89, 150)');
                        }
                        if ($(this).css('fill') == 'rgb(88, 45, 131)') {
                            $(this).css('fill', 'rgb(165, 29, 84)');
                        }
                        if ($(this).css('fill') == 'rgb(138, 107, 168)') {
                            $(this).css('fill', 'rgb(206, 89, 148)');
                        }
                    });
                    var rects = $(this).find('rect');
                    rects.each(function() {
                        if ($(this).css('fill') == 'rgb(182, 191, 52)') {
                            $(this).css('fill', 'rgb(165, 29, 83)');
                        }
                        if ($(this).css('fill') == 'rgb(219, 223, 135)') {
                            $(this).css('fill', 'rgb(206, 89, 149)');
                        }
                        if ($(this).css('fill') == 'rgb(192, 199, 78)') {
                            $(this).css('fill', 'rgb(165, 29, 82)');
                        }
                        if ($(this).css('fill') == 'rgb(149, 174, 52)') {
                            $(this).css('fill', 'rgb(206, 89, 150)');
                        }
                        if ($(this).css('fill') == 'rgb(88, 45, 131)') {
                            $(this).css('fill', 'rgb(165, 29, 84)');
                        }
                        if ($(this).css('fill') == 'rgb(138, 107, 168)') {
                            $(this).css('fill', 'rgb(206, 89, 148)');
                        }
                    });
                    var polygons = $(this).find('polygon');
                    polygons.each(function() {
                        if ($(this).css('fill') == 'rgb(182, 191, 52)') {
                            $(this).css('fill', 'rgb(165, 29, 83)');
                        }
                        if ($(this).css('fill') == 'rgb(219, 223, 135)') {
                            $(this).css('fill', 'rgb(206, 89, 149)');
                        }
                        if ($(this).css('fill') == 'rgb(192, 199, 78)') {
                            $(this).css('fill', 'rgb(165, 29, 82)');
                        }
                        if ($(this).css('fill') == 'rgb(149, 174, 52)') {
                            $(this).css('fill', 'rgb(206, 89, 150)');
                        }
                        if ($(this).css('fill') == 'rgb(88, 45, 131)') {
                            $(this).css('fill', 'rgb(165, 29, 84)');
                        }
                        if ($(this).css('fill') == 'rgb(138, 107, 168)') {
                            $(this).css('fill', 'rgb(206, 89, 148)');
                        }
                    });
                } else if (thisElement.hasClass('white')) {
                    var paths = $(this).find('path');
                    paths.each(function() {
                        $(this).attr('style', 'fill: #ffffff !important');
                    });
                    var rects = $(this).find('rect');
                    rects.each(function() {
                        $(this).attr('style', 'fill: #ffffff !important');
                    });
                    var polygons = $(this).find('polygon');
                    polygons.each(function() {
                        $(this).attr('style', 'fill: #ffffff !important');
                    });
                }
            }
        });

        graphic.find('#text > g').each(function() {
            if (!$(this).hasClass(showElements)) {
                $(this).attr('style', 'opacity: 0.3');
            }
            if ($(this).hasClass('tile')) {
                var linearGradients = $(this).find('linearGradient');
                linearGradients.each(function() {
                    $(this).find('stop').each(function() {
                        if ($(this).attr('style') == 'stop-color:#B6BF34') {
                            $(this).attr('style', 'stop-color:#ce5995');
                        }
                        if ($(this).attr('style') == 'stop-color:#7A9B3D') {
                            $(this).attr('style', 'stop-color:#a51d53');
                        }
                    });
                });
            }
            if (showElements == 'growthEasterseals' && $(this).hasClass('magenta')) {
                $(this).find('text').first().attr('style', 'fill: #A51D53 !important;');
            }
        });
    }
}
