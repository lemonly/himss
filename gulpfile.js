'use strict';

var gulp = require('gulp'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    connect = require('gulp-connect'),
    insert = require('gulp-insert'),
    minifyCss = require('gulp-minify-css'),
    open = require('gulp-open'),
    removehtml = require('gulp-remove-html'),
    replace = require('gulp-replace'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify');

var roots = {
    dev: './dev/',
    build: './build/',
    client: 'capital-one',
    project: 'skills-framework',
    version: '1',
    url: 'https://lemonly.com/'
};

gulp.task('connect', ['sass'], function() {
    connect.server({
        root: roots.dev,
        livereload: true
    });
});

gulp.task('setup', function() {
    return gulp.src(['./dev/index.html'])
               .pipe(replace('Project Title', roots.project + ' | ' + roots.client))
               .pipe(gulp.dest('./dev'));
});

gulp.task('html', function() {
    gulp.src(roots.dev + '*.html')
        .pipe(connect.reload());
});

gulp.task('sass', function () {
    gulp.src(roots.dev + 'scss/styles.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(roots.dev + 'css'));
});

gulp.task('css', function() {
    gulp.src(roots.dev + 'css/*.css')
        .pipe(connect.reload());
});

gulp.task('open', ['connect'], function() {
    gulp.src(__filename)
        .pipe(open({uri: 'http://0.0.0.0:8080'}));
});

gulp.task('server', ['open'], function() {
    gulp.watch([roots.dev + '**/*.html'], ['html']);
    gulp.watch([roots.dev + 'scss/**/*.scss'], ['sass']);
    gulp.watch([roots.dev + 'css/**/*.css'], ['css']);
});

gulp.task('embed', ['build'], function() {
    return gulp.src('./embed')
               .pipe(clean());
});

gulp.task('build', ['remove', 'url', 'prepend', 'append'], function() {
    return gulp.src(['./embed/css/styles.css', './embed/remove/index.html', './embed/prepend/TweenMax.min.js', './dev/js/vendor/ScrollMagic.min.js', './embed/append/main.js'])
               .pipe(concat(dateHash() + '.html'))
               .pipe(gulp.dest('./build/'));
});

gulp.task('remove', function() {
    var link = roots.url + roots.client.toLowerCase().replace(/\s/g, '-') + '/' + roots.project.toLowerCase().replace(/\s/g, '-') + '/v' + roots.version + '/img';

    return gulp.src(roots.dev + 'index.html')
               .pipe(removehtml())
               .pipe(replace('src="img', 'src="' + link))
               .pipe(replace('srcset="img', 'srcset="' + link))
               .pipe(replace('xlink:href="img', 'xlink:href="' + link))
               .pipe(gulp.dest('./embed/remove/'));
});

gulp.task('url', function() {
    var link = roots.url + roots.client.toLowerCase().replace(/\s/g, '-') + '/' + roots.project.toLowerCase().replace(/\s/g, '-') + '/v' + roots.version;

    return gulp.src([roots.dev + 'css/styles.css'])
               .pipe(minifyCss())
               .pipe(replace('url("../', 'url("' + link + '/'))
               .pipe(replace('url(../', 'url(' + link + '/'))
               .pipe(insert.prepend('<style>\n'))
               .pipe(insert.append('\n</style>'))
               .pipe(gulp.dest('./embed/css/'));
});

gulp.task('prepend', function() {
    return gulp.src(roots.dev + 'js/vendor/TweenMax.min.js')
               .pipe(uglify())
               .pipe(insert.prepend('<script>\n'))
               .pipe(gulp.dest('./embed/prepend/'));
});

gulp.task('append', function() {
    return gulp.src(roots.dev + 'js/main.js')
               .pipe(uglify())
               .pipe(insert.append('\n</script>'))
               .pipe(gulp.dest('./embed/append/'));
});

var dateHash = function() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    var date = yyyy + mm + dd + '_' + roots.client.replace(/\s/g, '') + '_' + roots.project.replace(/\s/g, '') + '_v' + roots.version;
    return date;
};
